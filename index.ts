import * as crypto from 'crypto';

// Transaktionen zwischen zwei Wallets
class Transaction {
  constructor(
    public amount: number, 
    public payer: string, // public key
    public payee: string // public key
  ) {}

  toString() {
    return JSON.stringify(this);
  }
}

// Individueller Block der Blockchain
class Block {

  public nonce = Math.round(Math.random() * 999999999);

  constructor(
    public prevHash: string, 
    public transaction: Transaction, 
    public ts = Date.now()
  ) {}

  get hash() {
    const str = JSON.stringify(this);
    const hash = crypto.createHash('SHA256');
    hash.update(str).end();
    return hash.digest('hex');
  }
}


// Die Blockchain
class Chain {
  // Singleton instance
  // eingesetztes Entwurfsmuster (Katergorie Erzeugungsmuster) - Stellt sicher, dass von einer Klasse genau ein globales Objekt existiert
  public static instance = new Chain(); 

  chain: Block[];

  constructor() {
    this.chain = [
      // Genesis Block
      new Block('', new Transaction(100, 'genesis', 'satoshi'))
    ];
  }

  // Letzter Block
  get lastBlock() {
    return this.chain[this.chain.length - 1];
  }

  // Proof of Work System - Zufallszahl
  mine(nonce: number) {
    let solution = 1;
    console.log('⛏️  mining...')

    while(true) {
      const hash = crypto.createHash('MD5');
      hash.update((nonce + solution).toString()).end();

      const attempt = hash.digest('hex');

      if(attempt.substr(0,4) === '0000'){
        console.log(`Solved: ${solution}`);
        return solution;
      }

      solution += 1;
    }
  }

  // Fügt einen neuen Block der Blockchain hinzu, wenn die Signatur valide ist & proof of work geleistet wurde 
  addBlock(transaction: Transaction, senderPublicKey: string, signature: Buffer) {
    const verify = crypto.createVerify('SHA256');
    verify.update(transaction.toString());

    const isValid = verify.verify(senderPublicKey, signature);

    if (isValid) {
      const newBlock = new Block(this.lastBlock.hash, transaction);
      this.mine(newBlock.nonce);
      this.chain.push(newBlock);
    }
  }

}

// Wallet gibt User ein Public/Private-Key Paar
class Wallet {
  public publicKey: string;
  public privateKey: string;

  constructor() {
    const keypair = crypto.generateKeyPairSync('rsa', {
      modulusLength: 2048,
      publicKeyEncoding: { type: 'spki', format: 'pem' }, // simple public key infrastructure
      privateKeyEncoding: { type: 'pkcs8', format: 'pem' }, // private key cryptographic standard - Standard Syntax um Private Key Informationen zu speichern
    });

    this.privateKey = keypair.privateKey;
    this.publicKey = keypair.publicKey;
  }

  sendMoney(amount: number, payeePublicKey: string) {
    const transaction = new Transaction(amount, this.publicKey, payeePublicKey);

    const sign = crypto.createSign('SHA256');
    sign.update(transaction.toString()).end();

    const signature = sign.sign(this.privateKey); 
    Chain.instance.addBlock(transaction, this.publicKey, signature);
  }
}

// Beispielverwendung

const jim = new Wallet();
const alice = new Wallet();
const tom = new Wallet();

jim.sendMoney(50, tom.publicKey);
tom.sendMoney(20, alice.publicKey);
alice.sendMoney(8, tom.publicKey);

console.log(Chain.instance)